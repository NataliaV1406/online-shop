$(document).ready(function() {
	const token = $('meta[name=csrf-token]').attr('content');

	$('.send').click(function(){
		let email = $(this).parent().find("#email").val();
		$.ajax({
			url: '/password-recover',
			type: 'post',
			data: {_token: token, email: email},
			success:(r)=>{
				let x= $(`<div class="col-sm-12">`);
				r.forEach(function(el, index){
					x.append(` <div class="form-group">
						@error('key')
						<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
						@enderror
						<label for="key">key:</label>
						<input type="text" id="key" class="form-control" name="key" placeholder="Enter key number">
						</div>
						<div class="form-group">
						@error('newpassword')
						<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
						@enderror
						<label for="newpassword">New password:</label>
						<input type="password" id="newpassword" class="form-control" name="newpassword" placeholder="Enter your new password">
						</div>
						<div class="form-group">
						@error('cpassword')
						<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
						@enderror
						<label for="cpassword">Confirm new password:</label>
						<input type="password" id="cpassword" class="form-control" name="cpassword" placeholder="Confirm your new password">
						</div>
						<div class="w-100"></div>
						<div class="col-sm-12">
						<div class="form-group">
						<input type="submit" value="save Forgot password" class="btn btn-primary saveForgot">
						</div>
						</div>`);
				})
			}
		})
		
		
	})

	$('.recover').click(function(){
		let email = $(this).parent().find("#email").val();
		$.ajax({
			url: '/password-recover',
			type: 'post',
			data: {_token: token, email: email},
			success:(r)=>{
				let x= $(`<div class="col-sm-12">`);
				r.forEach(function(el, index){
					x.append(` <div class="form-group">
						@error('key')
						<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
						@enderror
						<label for="key">key:</label>
						<input type="text" id="key" class="form-control" name="key" placeholder="Enter key number">
						</div>
						<div class="form-group">
						@error('newpassword')
						<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
						@enderror
						<label for="newpassword">New password:</label>
						<input type="password" id="newpassword" class="form-control" name="newpassword" placeholder="Enter your new password">
						</div>
						<div class="form-group">
						@error('cpassword')
						<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
						@enderror
						<label for="cpassword">Confirm new password:</label>
						<input type="password" id="cpassword" class="form-control" name="cpassword" placeholder="Confirm your new password">
						</div>
						<div class="w-100"></div>
						<div class="col-sm-12">
						<div class="form-group">
						<input type="submit" value="save Forgot password" class="btn btn-primary saveForgot">
						</div>
						</div>`);
				})
			}
		})
		
		
	})
});