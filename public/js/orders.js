$(document).ready(function() {
	const token = $('meta[name=csrf-token]').attr('content');

	$('.addfeedback').click(function(){
		let id = $(this).attr('data-id');
		$.ajax({
			url: '/addfeedback',
			type: 'post',
			data: {_token: token, id: id},
			success:(r)=>{
				console.log(r)
			}
		})
		
		
	})
	
	$('.savefeedback').click(function(){
		let id = $(this).attr('data-id');
		let message = $(".message").val();
		$.ajax({
			url: '/save-feedback',
			type: 'post',
			data: {_token: token, id: id, message: message},
			success:(r)=>{
				console.log(r)
			}
		})
		
		
	})
	$('.closed').click(function(){
		let id = $(this).attr('data-id');
		$.ajax({
			url: '/delete-order',
			type: 'post',
			data: {_token: token, id: id},
			success:(r)=>{
				console.log(r)
			}
		})
		
		
	})
});