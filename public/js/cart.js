$(document).ready(function() {
	const token = $('meta[name=csrf-token]').attr('content');

	$('.addtocart').click(function(){
		let id = $(this).attr('data-id');
		$.ajax({
			url: '/cart',
			type: 'post',
			data: {_token: token, id: id},
			success:(r)=>{
				console.log(r)
			}
		})
		
		
	})
	$('.moveToCart').click(function(){
		let id = $(this).attr('data-id');
		$.ajax({
			url: '/move-to-cart',
			type: 'post',
			data: {_token: token, id: id},
			success:(r)=>{
				console.log(r)
			}
		})
		
		
	})
	
	$('.closed').click(function(){
		let id = $(this).attr('data-id');
		$.ajax({
			url: '/delete-item',
			type: 'post',
			data: {_token: token, id: id},
			success:(r)=>{
				console.log(r)
			}
		})
		
		
	})
});