$(document).ready(function() {
	const token = $('meta[name=csrf-token]').attr('content');

	$('.addtowishlist').click(function(){
		let id = $(this).attr('data-id');
		$.ajax({
			url: '/add-to-wishlist',
			type: 'post',
			data: {_token: token, id: id},
			success:(r)=>{
				console.log(r)
			}
		})
		
		
	})
	
	$('.moveToWishlist').click(function(){
		let id = $(this).attr('data-id');
		$.ajax({
			url: '/move-to-wishlis',
			type: 'post',
			data: {_token: token, id: id},
			success:(r)=>{
				console.log(r)
			}
		})
		
		
	})
	$('.closed').click(function(){
		let id = $(this).attr('data-id');
		$.ajax({
			url: '/delete-item',
			type: 'post',
			data: {_token: token, id: id},
			success:(r)=>{
				console.log(r)
			}
		})
		
		
	})
});