
@extends("layouts.layout")
@section("title", "login")

@section("content")
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col">
						<p class="bread"><span><a href="{{url('/index')}}">Home</a></span> / <span>Log In</span></p>
					</div>
				</div>
			</div>
		</div>
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col">
		<h3 class="text-danger">{{session()->get("message")}}</h3>
					</div>
				</div>
			</div>
		</div>
		<div id="colorlib-contact">
			<div class="container">
				
				<div class="row">

					<div class="col-md-6">
						<div class="contact-wrap">
							<h3>Log in</h3>
							<form action="{{url("login")}}" method="post" class="contact-form">

								<div class="row">
									@csrf
									
									<div class="w-100"></div>
									<div class="col-sm-12">
										<div class="form-group">
											@error('email')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="email">Email</label>
											<input type="text" id="email" name="email" class="form-control" value="{{old('email')}}" placeholder="Your email address">
										</div>
									</div>
									<div class="w-100"></div>
									<div class="col-sm-12">
										<div class="form-group">
											@error('password')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="password">Password:</label>
											<input type="password" id="password" class="form-control" name="password" placeholder="Your password">
										</div>
									</div>
									<div class="w-100"></div>
													<div class="col-sm-12">
										<div class="form-group">
											<p ><a class="text-success" href="{{URL::to('/password-recover') }}">forgot password?</a></p>
										</div>
									</div>
									<div class="w-100"></div>
									<div class="col-sm-12">
										<div class="form-group">
											<input type="submit" value="Log In" class="btn btn-primary float-right">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	
@endsection	

