@extends("layouts.layout")
@section("title", "myProduct")

@section("content")
<div class="breadcrumbs">
	<div class="breadcrumbs">
		<div class="container">
			<div class="row">
				<div class="col">
					<p class="bread"><span><a href="{{url('/index')}}">Home</a></span> / <span>my Products</span></p>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-10">
				<h1 class="text-success">Hello, {{$user["name"]}}</h1>
			</div>
			
			<div class="container m-4">
				<div class="row">
					@foreach($products as $product)
					<div class="col-sm-6 text-center">
						<div class="featured">
							<div class="featured-img featured-img-2" style="background: url({{asset('product-photo/'.$product->photo[0]->url)}});background-size: cover;background-position: center;background-repeat: no-repeat;">
								<h3>Name: {{$product["name"]}}</h3>
								<h5>Count: {{$product["count"]}}</h5>
								<h5>Price: ${{$product["price"]}}</h5>
								<p style="color: #000">Description: {{$product["description"]}}</p>
								<p><a href="{{URL::to('/delete/'.$product->id) }}" class="btn btn-primary btn-lg">Delete</a></p>
								<p><a href="{{URL::to('/change/'.$product->id) }}" class="btn btn-success btn-lg">Edit</a></p>
							</div>
						</div>
					</div>
					@endforeach
					
				</div>
			</div>
			<div class="d-felx justify-content-center">

				{{ $products->links() }}

			</div>
			
		</div>
	</div>

</div>

	
@endsection	
