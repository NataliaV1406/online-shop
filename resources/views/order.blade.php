@extends("layouts.layout")
@section("title", "cart")

@section("content")
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col">
						<p class="bread"><span><a href="{{url('/index')}}">Home</a></span> / <span>Shopping Cart</span></p>
					</div>
				</div>
			</div>
		</div>
		<div class="colorlib-product">
			<div class="container">
				<div class="row row-pb-lg">
					<div class="col-md-10 offset-md-1">
						<div class="process-wrap">
							<div class="process text-center active">
								<p><span>01</span></p>
								<a href="{{url('/cart')}}"><h3>Shopping Cart</h3></a>
							</div>
							<div class="process text-center">
								<p><span>02</span></p>
								<a href="{{url('/checkout')}}"><h3>Checkout</h3></a>
							</div>
							<div class="process text-center">
								<p><span>03</span></p>
								<a href="{{url('/order-complete')}}"><h3>Order Complete</h3></a>
							</div>
						</div>
					</div>
				</div>
				<div class="row row-pb-lg">
					<div class="col-md-12">
						<div class="product-name d-flex">
							<div class="one-forth text-left px-4">
								<span>Client's name</span>
							</div>
							<div class="one-eight text-center">
								<span>Product's image</span>
							</div>
							<div class="one-eight text-center">
								<span>Product's name </span>
							</div>
							
							<div class="one-eight text-center">
								<span>Total</span>
							</div>
							
							<div class="one-eight text-center px-4">
								<span>Details</span>
							</div>
						</div>
						@foreach($orders as $order)
						<div class="product-cart d-flex">
							<div class="one-eight text-center">
								<div class="display-tc">
									<span class="price">{{$user['name']}} {{$user['name']}}</span>
								</div>
							</div>
							<div class="one-forth">
								<div class="product-img" style="background-image: url({{asset('product-photo/'.$order->product->photo[0]->url)}});">
								</div>
								
							</div>
							<div class="one-eight text-center">
								<div class="display-tc">
									<h3>{{$order->product['name']}}</h3>
								</div>
							</div>
							
							<div class="one-eight text-center">
								<div class="display-tc">
									<span class="price">${{$order->cart['count']*$order->product['price']}}</span>
								</div>
							</div>
							
							<div class="one-eight text-center">
								<div class="display-tc">
									<a href="{{URL::to('/order-detail/'.$product->id) }}" data-id="{{$order->product['id']}}" class="order_details btn btn-secondary text-light">details</a>
								</div>
							</div>
						</div>
					@endforeach

					@if(count($carts)>0)
					
					    <a href="{{url('/stripe')}}" class="btn btn-secondary text-light" ><h3>Checkout</h3></a>
				   
					@endif
					</div>
				</div>

				{{ $carts->links() }}


				<div class="row row-pb-lg">
					<div class="col-md-12">
						<div class="total-wrap">
							<div class="row">
								<div class="col-sm-8">
									<form action="#">
										<div class="row form-group">
											<div class="col-sm-9">
												<input type="text" name="quantity" class="form-control input-number" placeholder="Your Coupon Number...">
											</div>
											<div class="col-sm-3">
												<input type="submit" value="Apply Coupon" class="btn btn-primary">
											</div>
										</div>
									</form>
								</div>
								<div class="col-sm-4 text-center">
									<div class="total">
										<div class="sub">
											<p><span>Subtotal:</span> <span></span></p>
											<p><span>Delivery:</span> <span>$0.00</span></p>
											<p><span>Discount:</span> <span>$45.00</span></p>
										</div>
										<div class="grand-total">
											<p><span><strong>Total:</strong></span> <span>$450.00</span></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8 offset-sm-2 text-center colorlib-heading colorlib-heading-sm">
						<h2>Related Products</h2>
					</div>
				</div>
				<div class="row">
					@foreach($random as $r)
					<div class="col-md-3 col-lg-3 mb-4 text-center">
						<div class="product-entry border">
							<a href="{{URL::to('/product-detail/'.$r->id) }}" class="prod-img">
								<img src="{{asset('product-photo/'.$r->photo[0]->url)}}" class="img-fluid" alt="Free html5 bootstrap 4 template">
							</a>
							<div class="desc">
								<h2><a href="#">{{$r['name']}}</a></h2>
								<span class="price">${{$r['price']}}</span>
							</div>
						</div>
					</div>
					@endforeach
					
				</div>
			</div>
		</div>
		
