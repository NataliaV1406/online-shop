@extends("layouts.layout")
@section("title", "contact")

@section("content")
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col">
						<p class="bread"><span><a href="{{url('/index')}}">Home</a></span> / <span>Contact</span></p>
					</div>
				</div>
			</div>
		</div>
		<div id="colorlib-contact">
			<div class="container">
				
				<div class="row">
					<div class="col-md-6">
						<div class="contact-wrap">
							<h3>Feedback</h3>
							<form action="#" class="contact-form">
								<div class="row">
									@error('email')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
									<div class="col-sm-12">
										<div class="form-group">
											<label for="message">Message</label>
											<textarea name="message" id="message" cols="30" rows="10" class="form-control message" placeholder="Say something about us"></textarea>
										</div>
									</div>
									<div class="w-100"></div>
									<div class="col-sm-12">
										<div class="form-group">
											<input type="submit" value="Send" class="btn btn-primary savefeedback">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="col-md-6">
						<div id="map" class="colorlib-map"></div>
					</div>
				</div>
			</div>
		</div>
	
@endsection	