
@extends("layouts.layout")
@section("title", "registration")

@section("content")
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col">
						<p class="bread"><span><a href="{{url('/index')}}">Home</a></span> / <span>Registration</span></p>
					</div>
				</div>
			</div>
		</div>
		<div id="colorlib-contact">
			<div class="container">
				
				<div class="row">
					<div class="col-md-6">
						<div class="contact-wrap">
							<h3>Get In Touch</h3>
							<form action="{{url("add")}}" method="post" class="contact-form">

								<div class="row">
									@csrf
									<div class="col-md-6">
										<div class="form-group">
											@error('name')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="fname">First Name</label>
											<input type="text" id="fname" class="form-control" name="name" placeholder="Your firstname" value="{{old('name')}}">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											@error('surname')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="lname">Last Name</label>
											<input type="text" name="surname" id="lname" class="form-control" value="{{old('surname')}}" placeholder="Your lastname">
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											@error('age')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="age">Age</label>
											<input type="text" name="age" id="age" class="form-control" value="{{old('age')}}" placeholder="Your age">
										</div>
									</div>
									<div class="w-100"></div>
									<div class="col-sm-12">
										<div class="form-group">
											@error('email')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="email">Email</label>
											<input type="text" id="email" name="email" class="form-control" value="{{old('email')}}" placeholder="Your email address">
										</div>
									</div>
									<div class="w-100"></div>
									<div class="col-sm-12">
										<div class="form-group">
											@error('password')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="password">Password:</label>
											<input type="password" id="password" class="form-control" name="password" placeholder="Your password">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											@error('cpassword')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="cpassword">Confirm password:</label>
											<input type="password" id="cpassword" class="form-control" name="cpassword" placeholder="Confirm your password">
										</div>
									</div>
									<div class="w-100"></div>
									<div class="col-sm-12">
										<div class="form-group">
											<input type="submit" value="registration" class="btn btn-primary">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="col-md-6">
						<div id="map" class="colorlib-map"></div>
					</div>
				</div>
			</div>
		</div>
	
@endsection	




	