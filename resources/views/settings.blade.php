

@extends("layouts.layout")
@section("title", "profile")

@section("content")
<div class="breadcrumbs">
			<div class="container">
				
				</div>
					<div class="col-md-6">
						<div class="contact-wrap">
							<h3>Change:</h3>
							<form action="{{url("save")}}" method="post" class="contact-form">

								<div class="row">
									@csrf
									<div class="col-sm-12">
										<div class="form-group">
											@error('oldpassword')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="oldpassword">Old password:</label>
											<input type="password" id="oldpassword" class="form-control" name="oldpassword" placeholder="Enter your old password">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											@error('newpassword')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="newpassword">New password:</label>
											<input type="password" id="newpassword" class="form-control" name="newpassword" placeholder="Enter your new password">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											@error('cpassword')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="cpassword">Confirm new password:</label>
											<input type="password" id="cpassword" class="form-control" name="cpassword" placeholder="Confirm your new password">
										</div>
									</div>
									<div class="w-100"></div>
									<div class="col-sm-12">
										<div class="form-group">
											<input type="submit" value="save" class="btn btn-primary">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="colorlib-featured">
				
			</div>
		</div>
		
	
@endsection	
