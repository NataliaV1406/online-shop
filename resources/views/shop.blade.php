@extends("layouts.layout")
@section("title", "Shop")

@section("content")
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col">
						<p class="bread"><span><a href="{{url('/index')}}">Home</a></span> / <span>shop</span></p>
					</div>
				</div>
			</div>
		</div>
<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-10">
					<h1 class="text-success">Hello, {{$user["name"]}}</h1>
				    </div>
			
		   <div class="container m-4">
				<div class="row">
					@foreach($products as $product)
					<div class="col-sm-6 text-center">
						<div class="featured">
							<div class="featured-img featured-img-2" style="background: url({{asset('product-photo/'.$product->photo[0]->url)}});background-size: cover;background-position: center;background-repeat: no-repeat;">
								<h3>Name: {{$product["name"]}}</h3>
								
								<h5>Count: {{$product["count"]}}</h5>
								<h5>Price: ${{$product["price"]}}</h5>
								<p style="color: #000"> Description: {{$product["description"]}}</p>
								<div class="row">
								<div class="col-sm-12 text-center">
									<p><a data-id="{{$product->id}}" class="btn btn-danger btn-addtocart addtowishlist" data-id="{{$product->id}}"><i class="icon-heart" aria-hidden="true"></i></a></p>
								</div>
							    </div>
								<div class="row">
								<div class="col-sm-12 text-center">
									<p><a href="{{URL::to('/product-detail/'.$product->id) }}" class="btn btn-success btn-addtocart">Details</a></p>
								</div>
							    </div>
								
								
							</div>
						</div>
					</div>
					@endforeach
					
				</div>
			</div>
			<div class="d-felx justify-content-center">

				{{ $products->links() }}

			</div>
		</div>
	</div>
</div>

	
@endsection	
