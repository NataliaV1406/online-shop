@extends("layouts.layout")
@section("title", "forgot password")

@section("content")
<div class="breadcrumbs">
			<div class="container">
				
			
					<div class="col-md-6 m-auto">
						<div class="contact-wrap">
							<h3>write your e-mail:</h3>
							<form action="{{url("recover-code")}}" method="post" class="contact-form">

								<div class="row">
									@csrf
									<div class="col-sm-12">
										<div class="form-group">
											@error('code')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="code">Code:</label>
											<input type="text" id="code" class="form-control" name="code" placeholder="Enter code from mail">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											@error('newpassword')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="newpassword">New password:</label>
											<input type="password" id="newpassword" class="form-control" name="newpassword" placeholder="Enter your new password">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											@error('cpassword')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="cpassword">Confirm new password:</label>
											<input type="password" id="cpassword" class="form-control" name="cpassword" placeholder="Confirm your new password">
										</div>
									</div>
									
									<div class="w-100"></div>
									<div class="col-sm-12">
										<div class="form-group">
											<input type="submit" value="send " class="btn btn-primary send">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					</div>
			</div>

</div>
		
	
@endsection	
