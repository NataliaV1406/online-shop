@extends("layouts.layout")
@section("title", "profile")

@section("content")
<div class="breadcrumbs">
			<div class="container">
				<div class="col-md-3">
						<a href="{{url('/settings')}}" class="btn btn-primary"> Change password</a>
					</div>
				<div class="row">
					
				</div>
					<div class="col-md-6">
						<div class="contact-wrap">
							<h3>Add new product</h3>
							<form action="{{url("addproduct")}}" method="post" class="contact-form" enctype="multipart/form-data">

								<div class="row">
									@csrf
									<div class="col-md-6">
										<div class="form-group">
											@error('name')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="pname">Name:</label>
											<input type="text" id="pname" class="form-control" name="name" placeholder="Product's name" value="{{old('name')}}">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											@error('price')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="price">Price:</label>
											<input type="text" name="price" id="price" class="form-control" value="{{old('price')}}" placeholder="Price">
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											@error('count')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="count">Count:</label>
											<input type="text" name="count" id="count" class="form-control" value="{{old('count')}}" placeholder="Count">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											@error('description')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="description">Description:</label>
											<textarea type="text" id="description" name="description" class="form-control" value="{{old('description')}}" placeholder="Product's description"></textarea>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											@error('category')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="category">Category:</label>
											<select name="category" id="category" class="form-control" value="{{old('description')}}">
												<option selected disabled>Select Category: </option>
												@foreach($category as $item)
												<option value="{{$item['id']}}">{{$item['name']}}</option>
												@endforeach
											</select>
									</div>
									
									<div class="w-100"></div>
									<div class="col-sm-12">
										<div class="form-group">
											@error('photo')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="description">Photo:</label>
											<input type="file" id="photo" name="photo[]" class="form-control" value="{{old('photo')}}" placeholder="Enter photo" multiple accept="image/*">
										</div>
									</div>
									<div class="w-100"></div>
									<div class="col-sm-12">
										<div class="form-group">
											<input type="submit" value="Add" class="btn btn-primary">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="colorlib-featured">
				
			</div>
		</div>
		
	
@endsection	
