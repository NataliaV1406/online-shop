@extends("layouts.layout")
@section("title", "order details")

@section("content")
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col">
						<p class="bread"><span><a href="{{url('/index')}}">Home</a></span> / <span>order details</span></p>
					</div>
				</div>
			</div>
		</div>
		<div class="colorlib-product">
			<div class="container">
				
				<div class="row row-pb-lg">
					<div class="col-md-12">
						<div class="product-name d-flex">
							<div class="one-forth text-left px-4">
								<span>Product name</span>
							</div>
							<div class="one-eight text-center">
								<span>Price</span>
							</div>
							<div class="one-eight text-center">
								<span>Quantity</span>
							</div>
							<div class="one-eight text-center">
								<span>Total</span>
							</div>
							
							<div class="one-eight text-center px-4">
								<span>feedback</span>
							</div>
						</div>
						@foreach($order_details as $o)
						<div class="product-cart d-flex">
							<div class="one-forth">
								{{-- <div class="product-img" style="background-image: url({{asset('product-photo/'.$o->product->photo[0]->url)}});">
								</div> --}}
								<div class="display-tc">
									<h3>{{$o->product['name']}}</h3>
								</div>
							</div>
							<div class="one-eight text-center">
								<div class="display-tc">
									<span class="price">{{$o->product['price']}}</span>
								</div>
							</div>
							<div class="one-eight text-center">
								<div class="display-tc">
									<input type="text" id="quantity" name="quantity" class="form-control input-number text-center" value="{{$cart['count']}}" min="1" max="{{$o->product['count']}}">
								</div>
							</div>
							<div class="one-eight text-center">
								<div class="display-tc">
									<span class="price">${{$o->cart['count']*$o->product['price']}}</span>
								</div>
							</div>
							
							<div class="one-eight text-center">
								<div class="display-tc">
									<a data-id="{{$o->product['id']}}" class="addfeedback btn btn-secondary text-light">feedback</a>
								</div>
							</div>
						</div>
					@endforeach

					
				</div>

				


				
		
@endsection	