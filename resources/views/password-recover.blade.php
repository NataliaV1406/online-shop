@extends("layouts.layout")
@section("title", "forgot password")

@section("content")
<div class="breadcrumbs">
	<div class="container">
				<div class="row">
					<div class="col">
							<h3 class="text-danger">{{session()->get("message")}}</h3>
					</div>
				</div>
			</div>
			<div class="container">
				
			
					<div class="col-md-6 m-auto">
						<div class="contact-wrap">
							<h3>write your e-mail:</h3>
							<form action="{{url("send")}}" method="post" class="contact-form">

								<div class="row">
									@csrf
									<div class="col-sm-12">
										<div class="col-sm-12">
										<div class="form-group">
											@error('email')
											<div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
											@enderror
											<label for="email">Email</label>
											<input type="text" id="email" name="email" class="form-control" value="{{old('email')}}" placeholder="Your email address">
										</div>
									</div>
									
									<div class="w-100"></div>
									<div class="col-sm-12">
										<div class="form-group">
											<input type="submit" value="send " class="btn btn-primary send">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					</div>
			</div>

</div>
		
	
@endsection	
