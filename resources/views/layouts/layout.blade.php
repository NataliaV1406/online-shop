<!DOCTYPE HTML>
<html>
<head>
	<title>@yield("title")</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" id="csrf-token" content="{{csrf_token()}}">
	<link href="{{asset('../../css.css?family=Montserrat:300,400,500,600,700')}}" rel="stylesheet">
	<link href="{{asset('../../css-1.css?family=Rokkitt:100,300,400,700')}}" rel="stylesheet">

	<link rel="stylesheet" href="{{asset('css/animate.css')}}">

	<link rel="stylesheet" href="{{asset('css/icomoon.css')}}">

	<link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">

	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

	<link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">

	<link rel="stylesheet" href="{{asset('css/flexslider.css')}}">

	<link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">

	<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker.css')}}">

	<link rel="stylesheet" href="{{asset('fonts/flaticon/font/flaticon.css')}}">

	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<link rel="stylesheet" href="{{asset('https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"')}}"/>

	<link rel="stylesheet" href="{{asset('css/stripe.css')}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
	<div class="colorlib-loader"></div>
	<div id="page">
		<nav class="colorlib-nav" role="navigation">
			<div class="top-menu">
				<div class="container">
					<div class="row">
						<div class="col-sm-7 col-md-9">
							<div id="colorlib-logo"><a href="{{asset('index.html')}}">GRIGORIO</a></div>
						</div>
						<div class="col-sm-5 col-md-3">
							<form action="#" class="search-wrap">
								<div class="form-group">
									<input type="search" class="form-control search" placeholder="Search">
									<button class="btn btn-primary submit-search text-center" type="submit"><i class="icon-search"></i></button>
								</div>
							</form>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 text-left menu-1">
							<ul>
								<li class="active"><a href="{{url('/profile')}}">Home</a></li>
								{{-- <li class="has-dropdown">
									<a href="{{asset('men.html')}}">Men</a>
									<ul class="dropdown">
										<li><a href="{{url('/product-detail')}}">Product Detail</a></li>
										<li><a href="{{url('/cart')}}">Shopping Cart</a></li>
										<li><a href="{{url('/checkout')}}">Checkout</a></li>
										<li><a href="{{url('/order-complete')}}">Order Complete</a></li>
										<li><a href="{{url('/add-to-wishlist')}}">Wishlist</a></li>
									</ul>
								</li> --}}
								<li><a href="{{url('/shop')}}">shop</a></li>
								<li><a href="{{url('/about')}}">About</a></li>
								<li><a href="{{url('/contact')}}">Contact</a></li>
								@if (session()->has("id"))
								<li><a href="{{url('/add-to-wishlist')}}">wishlist</a></li>
								<li><a href="{{url('/order')}}">orders</a></li>
								<li class="cart"><a href="{{url('/logout')}}"> Log out</a></li>
								<li class="cart"><a href="{{url('/cart')}}"><i class="icon-shopping-cart"></i> Cart </a></li>
								<li class="cart"><a href="{{url('/login')}}"> Log in</a></li>
								@endif

							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="sale">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 offset-sm-2 text-center">
							<div class="row">
								<div class="owl-carousel2">
									<div class="item">
										<div class="col">
											<h3><a href="#">25% off (Almost) Everything! Use Code: Summer Sale</a></h3>
										</div>
									</div>
									<div class="item">
										<div class="col">
											<h3><a href="#">Our biggest sale yet 50% off all summer shoes</a></h3>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</nav>



		@yield('content')






		<footer id="colorlib-footer" role="contentinfo">
			<div class="container">
				<div class="row row-pb-md">
					<div class="col footer-col colorlib-widget">
						<h4>About Footwear</h4>
						<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life</p>
						<p>
							<ul class="colorlib-social-icons">
								<li><a href="#"><i class="icon-twitter"></i></a></li>
								<li><a href="#"><i class="icon-facebook"></i></a></li>
								<li><a href="#"><i class="icon-linkedin"></i></a></li>
								<li><a href="#"><i class="icon-dribbble"></i></a></li>
							</ul>

						</div>
						<div class="col footer-col colorlib-widget">
							<h4>Customer Care</h4>
							<p>
								<ul class="colorlib-footer-links">
									<li><a href="#">Contact</a></li>
									<li><a href="#">Returns/Exchange</a></li>
									<li><a href="#">Gift Voucher</a></li>
									<li><a href="#">Wishlist</a></li>
									<li><a href="#">Special</a></li>
									<li><a href="#">Customer Services</a></li>
									<li><a href="#">Site maps</a></li>
								</ul>

							</div>
							<div class="col footer-col colorlib-widget">
								<h4>Information</h4>
								<p>
									<ul class="colorlib-footer-links">
										<li><a href="#">About us</a></li>
										<li><a href="#">Delivery Information</a></li>
										<li><a href="#">Privacy Policy</a></li>
										<li><a href="#">Support</a></li>
										<li><a href="#">Order Tracking</a></li>
									</ul>

								</div>
								<div class="col footer-col">
									<h4>News</h4>
									<ul class="colorlib-footer-links">
										<li><a href="{{asset('blog.html')}}">Blog</a></li>
										<li><a href="#">Press</a></li>
										<li><a href="#">Exhibitions</a></li>
									</ul>
								</div>
								<div class="col footer-col">
									<h4>Contact Information</h4>
									<ul class="colorlib-footer-links">
										<li>291 South 21th Street, <br> Suite 721 New York NY 10016</li>
										<li><a href="tel://1234567920">+ 1235 2355 98</a></li>
										<li><a href="/cdn-cgi/l/email-protection#e38a8d858ca39a8c9691908a9786cd808c8e"><span class="__cf_email__" data-cfemail="026b6c646d427b6d7770716b76672c616d6f">[email&#160;protected]</span></a></li>
										<li><a href="#">yoursite.com</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="copy">
							<div class="row">
								<div class="col-sm-12 text-center">
									<p>
										<span>
											Copyright &copy;<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="../../index.htm" target="_blank">Colorlib</a>
										</span>
										<span class="block">Demo Images: <a href="{{url('../../directory/index')}}" target="_blank">Unsplash</a> , <a href="{{asset('http://pexels.com/')}}" target="_blank">Pexels.com</a></span>
									</p>
								</div>
							</div>
						</div>
					</footer>
				</div>
				<div class="gototop js-top">
					<a href="#" class="js-gotop"><i class="ion-ios-arrow-up"></i></a>
				</div>

				<script src="{{asset('js/jquery.min.js')}}"></script>

				<script src="{{asset('js/popper.min.js')}}"></script>

				<script src="{{asset('js/bootstrap.min.js')}}"></script>

				<script src="{{asset('js/jquery.easing.1.3.js')}}"></script>

				<script src="{{asset('js/jquery.waypoints.min.js')}}"></script>

				<script src="{{asset('js/jquery.flexslider-min.js')}}"></script>

				<script src="{{asset('js/owl.carousel.min.js')}}"></script>

				<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
				<script src="{{asset('js/magnific-popup-options.js')}}"></script>

				<script src="{{asset('js/bootstrap-datepicker.js')}}"></script>

				<script src="{{asset('js/jquery.stellar.min.js')}}"></script>

				<script src="{{asset('js/main.js')}}"></script>
				<script src="{{asset('js/cart.js')}}"></script>
				<script src="{{asset('js/wishlist.js')}}"></script>
				<script src="{{asset('js/orders.js')}}"></script>
				<script src="{{asset('js/stripe.js')}}"></script>
				<script type="text/javascript" src="{{asset('https://js.stripe.com/v2/')}}"></script>


				<script src="{{asset('js/forgot-password.js')}}"></script>
				<script async="" src="{{asset('../../gtag/js.js?id=UA-23581568-13')}}"></script>
				<script>
					window.dataLayer = window.dataLayer || [];
					function gtag(){dataLayer.push(arguments);}
					gtag('js', new Date());

					gtag('config', 'UA-23581568-13');
				</script>
			</body>
			</html>
