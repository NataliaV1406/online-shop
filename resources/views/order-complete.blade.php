﻿@extends("layouts.layout")
@section("title", "order-complete")

@section("content")
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col">
						<p class="bread"><span><a href="{{asset('index.html')}}">Home</a></span> / <span>Purchase Complete</span></p>
					</div>
				</div>
			</div>
		</div>
		<div class="colorlib-product">
			<div class="container">
				<div class="row row-pb-lg">
					<div class="col-sm-10 offset-md-1">
						<div class="process-wrap">
							<div class="process text-center active">
								<p><span>01</span></p>
								<h3>Shopping Cart</h3>
							</div>
							<div class="process text-center active">
								<p><span>02</span></p>
								<h3>Checkout</h3>
							</div>
							<div class="process text-center active">
								<p><span>03</span></p>
								<h3>Order Complete</h3>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-10 offset-sm-1 text-center">
						<p class="icon-addcart"><span><i class="icon-check"></i></span></p>
						<h2 class="mb-4">Thank you for purchasing, Your order is complete</h2>
						<p>
							<a href="{{asset('index.html')}}" class="btn btn-primary btn-outline-primary">Home</a>
							<a href="{{asset('shop.html')}}" class="btn btn-primary btn-outline-primary"><i class="icon-shopping-cart"></i> Continue Shopping</a>
						</p>
					</div>
				</div>
			</div>
		</div>
			<div class="container">
				<div class="row row-pb-md">
					<div class="col footer-col colorlib-widget">
						<h4>About Footwear</h4>
						<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life</p>
						<p>
							<ul class="colorlib-social-icons">
								<li><a href="#"><i class="icon-twitter"></i></a></li>
								<li><a href="#"><i class="icon-facebook"></i></a></li>
								<li><a href="#"><i class="icon-linkedin"></i></a></li>
								<li><a href="#"><i class="icon-dribbble"></i></a></li>
							</ul>

						</div>
						<div class="col footer-col colorlib-widget">
							<h4>Customer Care</h4>
							<p>
								<ul class="colorlib-footer-links">
									<li><a href="#">Contact</a></li>
									<li><a href="#">Returns/Exchange</a></li>
									<li><a href="#">Gift Voucher</a></li>
									<li><a href="#">Wishlist</a></li>
									<li><a href="#">Special</a></li>
									<li><a href="#">Customer Services</a></li>
									<li><a href="#">Site maps</a></li>
								</ul>

							</div>
							<div class="col footer-col colorlib-widget">
								<h4>Information</h4>
								<p>
									<ul class="colorlib-footer-links">
										<li><a href="#">About us</a></li>
										<li><a href="#">Delivery Information</a></li>
										<li><a href="#">Privacy Policy</a></li>
										<li><a href="#">Support</a></li>
										<li><a href="#">Order Tracking</a></li>
									</ul>

								</div>
								<div class="col footer-col">
									<h4>News</h4>
									<ul class="colorlib-footer-links">
										<li><a href="blog.html">Blog</a></li>
										<li><a href="#">Press</a></li>
										<li><a href="#">Exhibitions</a></li>
									</ul>
								</div>
								<div class="col footer-col">
									<h4>Contact Information</h4>
									<ul class="colorlib-footer-links">
										<li>291 South 21th Street, <br> Suite 721 New York NY 10016</li>
										<li><a href="tel://1234567920">+ 1235 2355 98</a></li>
										<li><a href="/cdn-cgi/l/email-protection#1871767e775861776d6a6b716c7d367b7775"><span class="__cf_email__" data-cfemail="422b2c242d023b2d3730312b36276c212d2f">[email&#160;protected]</span></a></li>
										<li><a href="#">yoursite.com</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="copy">
							<div class="row">
								<div class="col-sm-12 text-center">
									<p>
										<span>
											Copyright &copy;<script data-cfasync="false" src="{{asset('../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js')}}"></script><script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="{{asset('../../index.htm')}}" target="_blank">Colorlib</a>
										</span>
										<span class="block">Demo Images: <a href="{{asset('../../directory/index.htm')}}" target="_blank">Unsplash</a> , <a href="{{asset('http://pexels.com/')}}" target="_blank">Pexels.com</a></span>
									</p>
								</div>
							</div>
						</div>
					</footer>
				</div>
				<div class="gototop js-top">
					<a href="#" class="js-gotop"><i class="ion-ios-arrow-up"></i></a>
				</div>


				<script src="{{asset('js/jquery.min.js')}}"></script>

				<script src="{{asset('js/popper.min.js')}}"></script>

				<script src="{{asset('js/bootstrap.min.js')}}"></script>

				<script src="{{asset('js/jquery.easing.1.3.js')}}"></script>

				<script src="{{asset('js/jquery.waypoints.min.js')}}"></script>

				<script src="{{asset('js/jquery.flexslider-min.js')}}"></script>

				<script src="{{asset('js/owl.carousel.min.js')}}"></script>

				<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
				<script src="{{asset('js/magnific-popup-options.js')}}"></script>

				<script src="{{asset('js/bootstrap-datepicker.js')}}"></script>

				<script src="{{asset('js/jquery.stellar.min.js')}}"></script>

				<script src="{{asset('js/main.js')}}"></script>

				<script async="" src="{{asset('../../gtag/js.js?id=UA-23581568-13')}}"></script>
				<script>
					window.dataLayer = window.dataLayer || [];
					function gtag(){dataLayer.push(arguments);}
					gtag('js', new Date());

					gtag('config', 'UA-23581568-13');
				</script>
			</body>
			</html>
@endsection	