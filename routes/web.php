<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\WishlistController;
use App\Http\Controllers\OrdersController;


Route::get('/', [UserController::class, 'index']);
Route::get('/login', [UserController::class, 'login']);
Route::post('login', [UserController::class, 'log']);
Route::get('/index', [UserController::class, 'index2']);
Route::post('add', [UserController::class, 'add']);
Route::get('/about', [UserController::class, 'about']);
Route::get('/add-to-wishlist', [WishlistController::class, 'wishlist'])->middleware("islogin");
Route::post('/add-to-wishlist', [WishlistController::class, 'add_to_wishlist']);
Route::post('/move-to-cart', [CartController::class, 'moveToCart']);

Route::get('/cart', [CartController::class, 'cart'])->middleware("islogin");
Route::post('/cart', [CartController::class, 'add_to_cart']);
Route::post('/move-to-wishlis', [WishlistController::class, 'moveTowishlist']);

Route::get('/checkout', [OrdersController::class, 'checkout']);
Route::get('/checkout-end', [OrdersController::class, 'checkout_end']);
Route::get('/order', [OrdersController::class, 'order']);

Route::get('/order-details/{id}', [OrdersController::class, 'order_details']);
Route::post('/addfeedback', [OrdersController::class, 'addfeedback']);
Route::get('/save-feedback', [OrdersController::class, 'save_feedback']);
Route::post('/savefeedback', [OrdersController::class, 'savefeedback']);
Route::post('/delete-order', [OrdersController::class, 'delete']);


Route::get('/contact', [UserController::class, 'contact']);
Route::get('/men', [UserController::class, 'men']);
Route::get('/order-complete', [UserController::class, 'order_complete']);
Route::get('/women', [UserController::class, 'women']);
Route::get('/product-detail/{id}', [ProductController::class, 'product_detail']);
Route::get('/profile', [UserController::class, 'profile']);
Route::post('addproduct', [ProductController::class, 'addproduct']);
Route::get('/shop', [UserController::class, 'shop']);
Route::get('/myProduct', [UserController::class, 'myProduct'])->middleware("islogin");
Route::get('/logout', [UserController::class, 'logout']);
Route::get('/settings', [UserController::class, 'settings']);
Route::post('save', [UserController::class, 'save']);

Route::get('/change/{id}', [ProductController::class, 'change']);
Route::post('add-changes', [ProductController::class, 'saveChange']);
Route::post('/delete-item', [CartController::class, 'delete']);
Route::post('/delete-item', [WishlistController::class, 'delete']);
Route::get('activate/{id}/{hash}', [UserController::class, 'activate']);
Route::get('/password-recover', [UserController::class, 'passwordRecover']);

Route::post('send', [UserController::class, 'sendEmail']);
Route::get('/password-recover/{id}/{hash}', [UserController::class, 'recover_code']);
Route::post('recover-code', [UserController::class, 'reset_password']);

Route::get('/stripe', [OrdersController::class, 'stripe']);
Route::post('stripe', [OrdersController::class, 'stripePost']);