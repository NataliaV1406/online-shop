<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class wishlist extends Model
{
    use HasFactory;
    public $timestamps= false;
    function product(){
    	return $this->belongsTo("App\Models\Product", "product_id");
    }
    
}
