<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    public $timestamps= false;
    function photo(){
    	return $this->hasMany("App\Models\Photo", "product_id");
    }
    function category(){
    	return $this->belongsTo("App\Models\Category", "category_id");
    }
    function user(){
    	return $this->belongsTo("App\Models\User", "user_id");
    }
    function cart(){
        return $this->belongsTo("App\Models\Cart", "product_id");
    }
    function wishlist(){
        return $this->belongsTo("App\Models\Wishlist", "product_id");
    }
}

