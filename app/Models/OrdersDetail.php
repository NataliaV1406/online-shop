<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrdersDetail extends Model
{
    use HasFactory;
	public $timestamps= false;
    public $table = 'order_details';
    function product(){
    	return $this->belongsTo("App\Models\Product", "product_id");
    }
}
