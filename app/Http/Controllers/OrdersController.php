<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use App\Models\Category;
use App\Models\Photo;
use App\Models\Cart;
use App\Models\Wishlist;
use App\Models\Order;
use App\Models\OrdersDetail;
use Hash;
use Validator;
use Mail;
use Session;
use Stripe;

class OrdersController extends Controller
{
     function checkout(){
        $orders=Order::with("product")->where(['user_id'=>session()->get("id")])->get();
        return view('checkout')->with('orders', $orders);
    }
    
    public function stripe()
    {
    	$cart=Cart::where(['user_id'=>session()->get("id")])->get();
    	$sum=0;
    	foreach ($cart as $value) {
    		$sum+=$value->count*$value->product->price;
    	}
        return view('stripe')->with('total', $sum);
    }
  
  
    public function stripePost(Request $request)
    {
        $cart=Cart::where(['user_id'=>session()->get("id")])->get();
    	$sum=0;
    	foreach ($cart as $value) {
    		$sum+=$value->count*$value->product->price;
    	}

        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
                "amount" => $sum * 100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Test payment from itsolutionstuff.com." 
        ]);


    	$orders=new Order;
    	$orders->user_id=session()->get("id");
        $orders->date=date('Y-m-d');
		$orders->total=$sum;
		$orders->save();

		foreach ($cart as $value) {
			$order_details = new Order_detail();
			$order_details->order_id=$orders->id;
			$order_details->product_id=$value->product_id;
			$order_details->count=$value->count;

			$order_details->feedback="";
			$order_details->save();

			$value->product->count-=$value->count;
			$value->product->save();

		}
		Cart::where(['user_id'=>session()->get("id")])->delete();
        Session::flash('success', 'Payment successful!');
		return redirect('order-details');          
    }


    function order_details(){
    	$order_details=Order::where(['user_id'=>session()->get("id"), 'product_id'=>$r->input('id')])->first();
    	return view('order-details')->with('order_details', $order_details);
    }
    function addfeedback(Request $r){
    	$orders=Order::where(['user_id'=>session()->get("id"),'product_id'=>$r->input('id')])->first();

    	return redirect("/save-feedback");
    }
    function save_feedback(Request $r){
    	$validator = Validator::make($r->all(), [
    		'message' => 'required|max:100',

    	]);

    	if ($validator->fails()){
    		return redirect()->back()
    		->withErrors($validator)
    		->withInput();
    	}
    	else{
    		$order_details= Order_detail::find(['user_id'=>session()->get("id"),'product_id'=>$r->input('id')]);
    		$order_details->feedback=$r->input("message");
    		$order_details->save();
    	}
    	return ('/order-details');
    }
    function order(){
    	$orders=Order::where(['user_id'=>session()->get("id")])->get();
    	
    	return view('order')->with('orders', $orders);
    }
}
