<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use App\Models\Category;
use App\Models\Photo;
use App\Models\Cart;
use App\Models\Wishlist;
use App\Models\Order;


use Hash;
use Validator;
use Mail;

class UserController extends Controller
{
    function index(){
    	return view('registrationForm');
    }

    function add(Request $r){
        $validator = Validator::make($r->all(), [
            'name' => 'required|max:50',
            'surname' => 'required|max:50',
            'age' => 'required|numeric',
            'email' => 'email|required|max:50',
            'password' => 'required|max:61',
            'cpassword'=>'required|max:61|same:password'

        ]);



        if ($validator->fails()){
            return redirect('/')
                            ->withErrors($validator)
                            ->withInput();
        }
        else{
            $user= new User();
            $user->name=$r->input("name");
            $user->surname=$r->input("surname");
            $user->age=$r->input("age");
            $user->email=$r->input("email");
            $user->password=Hash::make($r->input("password"));
            $user->save();
            $data = [
                'name'=> $user->name,
                'surname'=> $user->surname,
                'id'=> $user->id,
                'hash'=> md5($user->id.$user->email)
            ];
            Mail::send('mail', $data, function($message) use($user) {
               $message->to($user->email, $user->name, $user->surname)->subject('Shop verification');
               $message->from('nv14061989@gmail.com',"Online shop");
      });
            return redirect("/login")->with("message","HTML Email Sent. Check your inbox.");
        }
    	
    }
    function login(){
        $user=User::find(session()->get("id"));
        return view('login');
    }
    function log(Request $r){
        $validator = Validator::make($r->all(), [
            
            'email' => 'email:rfc,dns,filter|required|max:50',
            'password' => 'required|max:61',
            
        ]);

        $user=User::where("email", $r->input("email"))->first();
        
        $validator->after(function ($validator)use($user, $r) {
            if (!$user) {
                $validator->errors()->add('email', 'Aytens mard chka!');
            }
            else if ($user->active == 0){
                $validator->errors()->add('email', 'Check your e-mail');
            }
            else if (!Hash::check($r->input("password"), $user->password)){
                $validator->errors()->add('password', 'Parol sxal e!');
            }

        });
        if ($validator->fails()){
            return redirect('/login')
                            ->withErrors($validator)
                            ->withInput();
        }
        else{
            session()->put("id", $user->id);
            return redirect("/myProduct");
        }
        
    }

    function about(){
        return view('about');
    }
    function index2(){
        return view('index2');
    }
    
   
    function contact(){
        return view('contact');
    }
    function men(){
        $products=Product::get();
        return view('men')->with('products', $products);
    }
    function order_complete(){
        return view('order-complete');
    }
    function women(){
        $products=Product::get();
        return view('women')->with('products', $products);
    }
   
     function profile(){
        $user=User::find(session()->get("id"));
        $category=Category::get();
        return view('profile')
                    ->with("user", $user)
                    ->with("category", $category);
    }
    function shop(){
        $user=User::find(session()->get("id"));
        $category=Category::get();
        $products=Product::with("photo", "category", "user")->paginate(4);
        return view('shop')
                    ->with("user", $user)
                    ->with("category", $category)
                    ->with("products", $products);
    }
    function myProduct(){
        $user=User::find(session()->get("id"));
        $category=Category::get();
        
        $products=Product::where('user_id',session()->get("id"))->with("photo", "category", "user")->paginate(4);
        
        return view('myProduct')
                    ->with("user", $user)
                    ->with("category", $category)
                    ->with("products", $products);
    }

    function logout(){
        session()->forget('id');

            return redirect("/login");
    }
    function settings(){
       return view('/settings');

   }
   function save(Request $r){
       $validator = Validator::make($r->all(), [
        'oldpassword' => 'required|max:61',
        'newpassword' => 'required|max:61',
        'cpassword'=>'required|max:61|same:newpassword'

    ]);

       $user=User::find(session()->get("id"));
       $validator->after(function ($validator)use($user, $r) {
        if ($r->input("oldpassword")==$r->input("newpassword")){
            $validator->errors()->add('newpassword', 'nuyn en');
        }
        else if (!Hash::check($r->input("oldpassword"), $user->password)){
            $validator->errors()->add('oldpassword', 'Hin parol sxal e!');
        }


         });
           if ($validator->fails()){
            return redirect('/settings')
            ->withErrors($validator)
            ->withInput();
        }
        else{
            $user->password = Hash::make($r->input("newpassword"));
            $user->save();
            return redirect("/profile");
        }


    }
    function delete($id){
        $products=Product::find($id)->delete();
        return redirect("/shop");
    }
    function activate($id, $hash){
        $user=User::find($id);
        if ($user){
            if ($hash == md5($user->id.$user->email)){
                $user->active=1;
                $user->save();
            }
        }
        return redirect('/login')->with("message", "Thanks");
    }
    function passwordRecover(){
        return view('password-recover');
    }
    function sendEmail(Request $r){
       
        $validator = Validator::make($r->all(), [
            
            'email' => 'email|required|max:50',
     
        ]);

        if ($validator->fails()){
            return redirect('/')
                            ->withErrors($validator)
                            ->withInput();
        }
        else{
           
             $code = mt_rand(1000000, 9999999)
                . mt_rand(1000000, 9999999);
                
            $string = str_shuffle($code);
            $user=User::where("email", $r->input("email"))->first();
            $data = [
                'name'=> $user->name,
                'surname'=> $user->surname,
                'id'=> $user->id,
                'hash'=> md5($user->id.$user->email),
                'code'=> $string
            ];
            Mail::send('recovermail', $data, function($message) use($user) {
               $message->to($user->email, $user->name, $user->surname, $user->code)->subject('You can change password');
               $message->from('nv14061989@gmail.com',"Online shop");
      });
            session()->put("code", $string);
            session()->put("email", $user->email);
            return redirect()->back()->with("message","HTML Email Sent. Check your inbox.");
        }
        
    }
    function recover_code(){

         return view('recover-code');
    }
     function reset_password(Request $r){
         $validator = Validator::make($r->all(), [
        
        'newpassword' => 'required|max:61',
        'cpassword'=>'required|max:61|same:newpassword'

    ]);
        $code = session()->get('code');
        $email=session()->get('email');
        $user=User::where("email", $email)->first();
        $validator->after(function ($validator)use($user, $r, $code, $email) {
        if ($r->input("code") != $code){
            $validator->errors()->add('code', 'code is wrong');
        }
         });
           if ($validator->fails()){
            return redirect('/recover-code')
            ->withErrors($validator)
            ->withInput();
        }
        else{
            $user->password = Hash::make($r->input("newpassword"));
            $user->save();

            return redirect("/login")->with("message","HTML Password is changed. Log In");
        }

     }
    
}
