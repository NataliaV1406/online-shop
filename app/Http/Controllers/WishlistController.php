<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use App\Models\Category;
use App\Models\Photo;
use App\Models\Cart;
use App\Models\Wishlist;


class WishlistController extends Controller
{
    public $timestamps= false;
	
    function add_to_wishlist(Request $r){
    	$wishlist=Wishlist::where(['user_id'=>session()->get("id"),'product_id'=>$r->input('id')])->first();
        if (!$wishlist) {
            $wishlist=new Wishlist;
            $wishlist->user_id=session()->get("id");
            $wishlist->product_id=$r->input('id');
            $wishlist->save();
            
        }
        return "ok";//redirect дописати

    }
     function wishlist(){
        $wishlist=Wishlist::with("product")->where('user_id',session()->get("id"))->paginate(7);
        return view('add-to-wishlist')->with('wishlist', $wishlist);
     }
     function delete(Request $r){
        $wishlist=Wishlist::where(['user_id'=>session()->get("id"),'product_id'=>$r->input('id')])->delete();
        return redirect("/add-to-wishlist");
    }
    function moveTowishlist(Request $r){
        $wishlist=Wishlist::where(['user_id'=>session()->get("id"),'product_id'=>$r->input('id')])->first();
        if (!$wishlist) {
            $wishlist=new Wishlist;
            $wishlist->user_id=session()->get("id");
            $wishlist->product_id=$r->input('id');
            $wishlist->save();
            
        }
        Cart::where(['user_id'=>session()->get("id"),'product_id'=>$r->input('id')])->delete();
        return 'ok';
        
    }
}
