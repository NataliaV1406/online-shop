<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use App\Models\Category;
use App\Models\Photo;
use App\Models\Cart;
use App\Models\Wishlist;
use link;

class CartController extends Controller
{	
	
    function add_to_cart(Request $r){

    	$carts=Cart::where(['user_id'=>session()->get("id"),'product_id'=>$r->input('id')])->first();
    	if ($carts) {
    		$carts->count++;
    		$carts->save();
    	}
    	else{
	    	$carts=new Cart;
	    	$carts->user_id=session()->get("id");
	    	$carts->product_id=$r->input('id');
	    	$carts->count=1;
	    	$carts->save();
    		
    	}
    	return "ok";//redirect дописати
    	
    }

    function cart(){
    	$carts=Cart::with("product")->where('user_id',session()->get("id"))->paginate(7);
        $random=Product::inRandomOrder()
                ->limit(4)
                ->get();
        return view('cart')->with('carts', $carts)->with('random', $random);
    }
    function delete(Request $r){
        $carts=Cart::where(['user_id'=>session()->get("id"),'product_id'=>$r->input('id')])->delete();
        return redirect("/cart");
    }
     function moveToCart(Request $r){
        
        $carts=Cart::where(['user_id'=>session()->get("id"),'product_id'=>$r->input('id')])->first();
        if ($carts) {
            $carts->count++;
            $carts->save();
        }
        else{
            $carts=new Cart;
            $carts->user_id=session()->get("id");
            $carts->product_id=$r->input('id');
            $carts->count=1;
            $carts->save();
            
        }
        Wishlist::where(['user_id'=>session()->get("id"),'product_id'=>$r->input('id')])->delete();
        return 'ok';
    }
}
