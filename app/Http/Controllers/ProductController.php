<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Photo;
use App\Models\User;
use App\Models\Cart;
use App\Models\Wishlist;
use Hash;
use Validator;

class ProductController extends Controller
{
    function addproduct(Request $r){
        
        $validator = Validator::make($r->all(), [
            'name' => 'required|max:100',
            'count' => 'required|numeric',
            'price' => 'required|numeric',
            'description' => 'required|max:255',
            'category' => 'required',
            'photo' => 'required',
            'photo.*' => 'mimes:jpg, png, gif, jpeg, webp, svg'
        ]);



        if ($validator->fails()){
            return redirect('/profile')
                            ->withErrors($validator)
                            ->withInput();
        }
        else{
            $products= new Product();
            $products->name=$r->input("name");
            $products->count=$r->input("count");
            $products->price=$r->input("price");
            $products->description=$r->input("description");
            $products->category_id=$r->input("category");
            $products->user_id=session()->get('id');
            $products->save();
            if($r->hasfile('photo'))
            {
                foreach($r->file('photo') as $file)
                {
                    $name = time().'.'.$file->getClientOriginalName();
                    $file->move(public_path().'/product-photo/', $name);  
                    $p=new Photo();
                    $p->url=$name;
                    $p->product_id=$products->id;
                    $p->save();
                }
            }
            return redirect('/myProduct');
        }
    	
    }
    function change($id){
            $user=User::find(session()->get("id"));
            $category=Category::get();
            $products=Product::with("photo", "category", "user")->find($id);
            
            return view('/add-changes')
                    ->with("user", $user)
                    ->with("category", $category)
                    ->with("product", $products);
    
        }

     function saveChange(Request $r){
        $validator = Validator::make($r->all(), [
            'name' => 'required|max:100',
            'count' => 'required|numeric',
            'price' => 'required|numeric',
            'description' => 'required|max:255',
            'category' => 'required',
            'photo.*' => 'mimes:jpg, png, gif, jpeg, webp, svg'
        ]);



        if ($validator->fails()){
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        }
        else{
            $products= Product::find($r->input('id'));
            $products->name=$r->input("name");
            $products->count=$r->input("count");
            $products->price=$r->input("price");
            $products->description=$r->input("description");
            $products->category_id=$r->input("category");
            $products->save();
            if($r->hasfile('photo'))
            {
                foreach($r->file('photo') as $file)
                {
                    $name = time().'.'.$file->getClientOriginalName();
                    $file->move(public_path().'/product-photo/', $name);  
                    $p=new Photo();
                    $p->url=$name;
                    $p->product_id=$products->id;
                    $p->save();
                }
            }

            return redirect('/shop');
        }
        }
     function product_detail($id){
        $products=Product::with("photo", "category", "user")->find($id);
        return view('product-detail')->with('products', $products);
    }
}
